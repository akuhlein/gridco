package com.gridco.client.commons.Resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface AppResource extends ClientBundle {
		  		  
		  @Source("power.png")
		  ImageResource power();
		  
		  @Source("admin.png")
		  ImageResource admin();
		  
		  @Source("alarm.png")
		  ImageResource alarm();
		  
		  @Source("controller.png")
		  ImageResource controller();
		  
		  @Source("ipr.png")
		  ImageResource ipr();
		  
		  @Source("logs.png")
		  ImageResource logs();
		  
		  @Source("software.png")
		  ImageResource software();
		  
		  @Source("header.css")
		  Header headerCss();
}
