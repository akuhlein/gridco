package com.gridco.client.commons.Resources;

import com.google.gwt.resources.client.CssResource;

public interface Header extends CssResource {
	
	@ClassName("logo")
	String logo();
	
	@ClassName("header-button")
	String headerBtn();
	
	@ClassName("header-toolbar")
	String toolBar();
	
	@ClassName("statusBox")
	String statusBox();
}
