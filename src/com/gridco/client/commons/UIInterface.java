package com.gridco.client.commons;


public interface UIInterface {
	/** 
	 * Associates the presenter with the current UI layer implementation.
	 * @param pPresenter
	 */
	void setPresenter(Presenter pPresenter);
	
	/**
	 * Calls to the UI layer in order to apply the language specific labels on the UI components.
	 */
	void updateLabels();
	
	/**
	 * Call to the UI layer in order to make it disable or active its components related events. 
	 * 
	 * @param pDisable TRUE if events must be deactived. FALSE in order to active them.
	 */
	void disableEvents(boolean pDisable);
	
	/**
	 * Call to the UI layer in order to make it register its components events. 
	 */
	void registerEvents();
}
