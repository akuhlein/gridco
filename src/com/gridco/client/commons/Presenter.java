package com.gridco.client.commons;

import com.google.gwt.event.shared.HandlerManager;

public interface Presenter {
	void loadDatas();
	
	void registerApplicationEvents(HandlerManager pEventBus);

	void updateLabels();

	void registerEvents();
}
