package com.gridco.client.commons.properties;

import com.google.gwt.editor.client.Editor.Path;
import com.gridco.shared.DemoDto;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface DemoProperties extends PropertyAccess<DemoDto> {

	@Path("id")
	ModelKeyProvider<DemoDto> key();

	@Path("name")
	LabelProvider<DemoDto> nameLabel();
	
	ValueProvider<DemoDto, Long> id();
	ValueProvider<DemoDto, String> name();

}
