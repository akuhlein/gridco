package com.gridco.client.module.main;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.gridco.client.commons.UIInterface;
import com.gridco.client.module.AbstractPresenter;
import com.gridco.client.module.AppController;

public class MainPresenter extends AbstractPresenter {

	
	public MainPresenter(AppController appController, UIInterface uiInterface) {
		super(appController, uiInterface);
		// TODO Auto-generated constructor stub
	}

	public void onAppChange(String string) {
		History.newItem(string);		
	}

	public void setApp(Composite view, int tabIndex) {
		((MainUI)uiInterface).setApp(view, tabIndex);
	}
}
