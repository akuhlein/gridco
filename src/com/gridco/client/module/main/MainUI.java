package com.gridco.client.module.main;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.gridco.client.commons.Presenter;
import com.gridco.client.commons.UIInterface;

public class MainUI implements UIInterface{

	MainView view;
	MainPresenter presenter;
	
	public MainUI(MainView view) {
		this.view = view;
	}
	
	@Override
	public void setPresenter(Presenter pPresenter) {
		presenter = (MainPresenter)pPresenter;
	}

	@Override
	public void updateLabels() {
	}

	@Override
	public void registerEvents() {
		view.getAdmin().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				History.newItem("0");
			}
		});
	}

	@Override
	public void disableEvents(boolean pDisable) {
		// TODO Auto-generated method stub
		
	}

	public void setApp(Composite view2, int tabIndex) {
		view.getModulePanel().setWidget(view2);
	}

}
