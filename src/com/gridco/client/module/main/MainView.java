package com.gridco.client.module.main;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;
import com.gridco.client.commons.Resources.AppResource;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Status;
import com.sencha.gxt.widget.core.client.Status.BoxStatusAppearance;
import com.sencha.gxt.widget.core.client.Status.StatusAppearance;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData;

public class MainView extends Composite {
	
	interface MainViewBinder extends UiBinder<Widget, MainView> {
		
	}
	
	private static MainViewBinder uiBinder = GWT.create(MainViewBinder.class);
	private static AppResource resource = GWT.create(AppResource.class);
	
	@UiField
	BorderLayoutData northData;
	
	@UiField
	ContentPanel modulePanel;
	
	
	@UiField(provided = true)
	Status statusDevice = new Status(GWT.<StatusAppearance> create(BoxStatusAppearance.class));
	
	@UiField(provided = true)
	Status statusActive = new Status(GWT.<StatusAppearance> create(BoxStatusAppearance.class));
	
	@UiField
	FocusPanel admin;
	
	public MainView() {
		resource.headerCss().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
	}

	public static MainViewBinder getUiBinder() {
		return uiBinder;
	}

	public static AppResource getResource() {
		return resource;
	}

	public BorderLayoutData getNorthData() {
		return northData;
	}

	public ContentPanel getModulePanel() {
		return modulePanel;
	}
	
	public FocusPanel getAdmin() {
		return admin;
	}
}