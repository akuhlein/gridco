package com.gridco.client.module;

import com.google.gwt.event.shared.HandlerManager;
import com.gridco.client.commons.Presenter;
import com.gridco.client.commons.UIInterface;

public class AbstractPresenter implements Presenter{

	protected AppController appController;
	protected UIInterface uiInterface;
	
	public AbstractPresenter(AppController appController, UIInterface uiInterface) {
		this.uiInterface = uiInterface;
		this.appController = appController;
		uiInterface.setPresenter(this);
		
		// Call the present to register to application wide events.
    	registerApplicationEvents(appController.getEventBus());
    	
    	registerEvents();

    	// Prevent events firing by UI components while loading data.
//    	uiInterface.disableEvents(true);
    	
    	// Call the UI layer to apply the labels to the components using the user language specified.
    	updateLabels();

	}
	
	@Override
	public void registerEvents() {
		uiInterface.registerEvents();		
	}
	
	@Override
	public void updateLabels() {
		uiInterface.updateLabels();		
	}
	
	public AppController getAppController() {
		return appController;
	}

	@Override
	public void loadDatas() {
		
	}

	@Override
	public void registerApplicationEvents(HandlerManager pEventBus) {
		
	}
}
