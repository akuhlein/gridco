package com.gridco.client.module.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.gridco.client.commons.properties.DemoProperties;
import com.gridco.client.commons.properties.StockProperties;
import com.gridco.shared.DemoDto;
import com.gridco.shared.Stock;
import com.gridco.shared.TestData;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;

public class AdminView extends Composite {

	private static AdminUiBinder uiBinder = GWT.create(AdminUiBinder.class);
	private DemoProperties properties = GWT.create(DemoProperties.class);
	private ListStore<DemoDto> listStore = new ListStore<DemoDto>(properties.key());
	private static final StockProperties props = GWT.create(StockProperties.class);
	
	private ColumnModel<Stock> cm;
	private ListStore<Stock> store;

	interface AdminUiBinder extends UiBinder<Widget, AdminView> {
	}
	
	@UiField
	TabPanel tab;
	
	@UiField(provided = true)
	ComboBox<DemoDto> combo = new ComboBox<DemoDto>(listStore, properties.nameLabel());
	
	@UiField(provided = true)
	Grid<Stock> grid;
	
	
	
	public AdminView() {
		final NumberFormat number = NumberFormat.getFormat("0");

	    ColumnConfig<Stock, String> nameCol = new ColumnConfig<Stock, String>(props.name(), 75, "UserName");
	    ColumnConfig<Stock, String> symbolCol = new ColumnConfig<Stock, String>(props.symbol(), 100, "Description");
	    ColumnConfig<Stock, Double> lastCol = new ColumnConfig<Stock, Double>(props.last(), 75, "Permission");
	    lastCol.setCell(new AbstractCell<Double>() {
	    	@Override
	    	public void render(com.google.gwt.cell.client.Cell.Context context,Double value, SafeHtmlBuilder sb) {
	    		sb.appendHtmlConstant(number.format(value));
	    	}
		});
	    ColumnConfig<Stock, Date> lastTransCol = new ColumnConfig<Stock, Date>(props.lastTrans(), 150, "Last Login");
		
		List<ColumnConfig<Stock, ?>> l = new ArrayList<ColumnConfig<Stock, ?>>();
	    l.add(nameCol);
	    l.add(symbolCol);
	    l.add(lastCol);	
	    l.add(lastTransCol);
	    cm = new ColumnModel<Stock>(l);
	    
	    store = new ListStore<Stock>(props.key());
	    store.addAll(TestData.getStocks());

	    grid = new  Grid<Stock>(store, cm);
	    grid.getView().setStripeRows(true);
	    grid.getView().setColumnLines(true);
	    grid.setHeight(160);
	    grid.setWidth(430);
	    combo.getStore().addAll(DemoDto.getRefreshTime());
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public TabPanel getTab() {
		return tab;
	}
}
