package com.gridco.client.module.admin;

import com.google.gwt.user.client.History;
import com.gridco.client.commons.UIInterface;
import com.gridco.client.module.AbstractPresenter;
import com.gridco.client.module.AppController;

public class AdminPresenter extends AbstractPresenter {

	
	public AdminPresenter(AppController appController, UIInterface uiInterface) {
		super(appController, uiInterface);
		// TODO Auto-generated constructor stub
	}

	public void onAppChange(String string) {
		History.newItem(string);		
	}
}
