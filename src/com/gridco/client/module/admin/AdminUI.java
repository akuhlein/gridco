package com.gridco.client.module.admin;

import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.gridco.client.commons.Presenter;
import com.gridco.client.commons.UIInterface;
import com.sencha.gxt.widget.core.client.event.ShowEvent;
import com.sencha.gxt.widget.core.client.event.ShowEvent.ShowHandler;

public class AdminUI implements UIInterface{

	AdminView view;
	AdminPresenter presenter;
	
	public AdminUI(AdminView view) {
		this.view = view;
	}
	
	@Override
	public void setPresenter(Presenter pPresenter) {
		presenter = (AdminPresenter)pPresenter;
	}

	@Override
	public void updateLabels() {
	}

	@Override
	public void registerEvents() {

		Window.addResizeHandler(new ResizeHandler() {
			
			Timer resizeTimer = new Timer() {  
			    @Override
			    public void run() {
			    	System.out.println(view.getTab().getParent().getParent() + " - " + view.getTab().getParent().getParent().getOffsetHeight());
					view.getTab().setHeight(view.getTab().getParent().getParent().getOffsetHeight());
			    }
			  };
			  
			@Override
			public void onResize(ResizeEvent event) {
				resizeTimer.cancel();
			    resizeTimer.schedule(250);
			}
		});
		
		view.getTab().addShowHandler(new ShowHandler() {
			
			@Override
			public void onShow(ShowEvent event) {
				System.out.println(view.getTab().getParent().getParent() + " - " + view.getTab().getParent().getParent().getOffsetHeight());
				view.getTab().setHeight(view.getTab().getParent().getParent().getOffsetHeight());
			}
		});
	}

	@Override
	public void disableEvents(boolean pDisable) {
		// TODO Auto-generated method stub
		
	}

}
