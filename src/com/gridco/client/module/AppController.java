package com.gridco.client.module;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.RootPanel;
import com.gridco.client.commons.Resources.AppResource;
import com.gridco.client.module.admin.AdminPresenter;
import com.gridco.client.module.admin.AdminUI;
import com.gridco.client.module.admin.AdminView;
import com.gridco.client.module.main.MainPresenter;
import com.gridco.client.module.main.MainUI;
import com.gridco.client.module.main.MainView;

public class AppController {

	private HandlerManager eventBus;
	private MainView mainView = null;
	private AdminView adminView = null;
	private MainPresenter mainPresenter = null;
	private List<AbstractPresenter> adminPresenter;
	int tabIndex = 0;
	public static AppResource icons = GWT.create(AppResource.class);
	
	public AppController(HandlerManager eventBus) {
		this.eventBus = eventBus;
		History.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				try {
					tabIndex = Integer.parseInt(event.getValue());
				} catch (Exception e) {
					tabIndex = 0;
				}
				switchApp();
			}
		});		
		showAppUI();
	}

	public void showAppUI() {
		if(mainView==null) {
			GWT.runAsync(new RunAsyncCallback() {
				@Override
				public void onSuccess() {
					mainView = new MainView();
					mainPresenter = new MainPresenter(getInstance(), new MainUI(mainView));
					showAppUISuccess();
				}
				@Override
				public void onFailure(Throwable reason) {
				}
			});
		} else {
			showAppUISuccess();
		}
	}
	
	private void showAppUISuccess() {
		RootPanel rootPanel = RootPanel.get();
		rootPanel.clear();
	    rootPanel.add(mainView);
	    try {
			tabIndex = Integer.parseInt(History.getToken());
		} catch (Exception e) {
			tabIndex = 0;			
		}
	    switchApp();
	}
	
	public void switchApp() {
		if(tabIndex==0)
			showAdminModule();
	}
	
	
	private void showAdminModule() {
		if(adminView == null) {
			GWT.runAsync(new RunAsyncCallback() {
				@Override
				public void onSuccess() {
					adminView = new AdminView();
					bindAdminModule(adminView);
					mainPresenter.setApp(adminView, tabIndex);
				}
				@Override
				public void onFailure(Throwable reason) {
				}
			});
		} else {
			mainPresenter.setApp(adminView, tabIndex);
		}
	}
	
	private void bindAdminModule(AdminView adminView) {
		adminPresenter = new ArrayList<AbstractPresenter>();

		AdminPresenter presenter = new AdminPresenter(getInstance(), new AdminUI(adminView));
		adminPresenter.add(presenter);
	}
	
	public AppController getInstance(){
		return this;
	}
	
	public HandlerManager getEventBus() {
		return eventBus;
	}
}
