package com.gridco.shared;

import java.io.Serializable;
import java.util.ArrayList;

public class DemoDto implements Serializable{

	private Long id;
	private String name;
	
	public DemoDto() {
		// TODO Auto-generated constructor stub
	}
	
	public DemoDto(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static ArrayList<DemoDto> getRefreshTime() {
		ArrayList<DemoDto> list = new ArrayList<DemoDto>();
		list.add(new DemoDto(1L, "30 seconds"));
		list.add(new DemoDto(2L, "40 seconds"));
		list.add(new DemoDto(3L, "50 seconds"));
		list.add(new DemoDto(4L, "60 seconds"));
		return list;
	}
}
